//Avery Headington
//Simple Calculator

#include <iostream>
#include <conio.h>

using namespace std;


float Number;
float num1;
float num2;
float denominator;
float numerator;

char Input;


float Added(float num1, float num2) 
{
	return num1 + num2;
}

float Subtract(float num1, float num2)
{
	return num1 - num2;
}

float Multiply(float num1, float num2)
{
	return num1 * num2;
}

bool Divide(float numerator, float denominator, float& result)
{
	if (denominator == 0) return false;
	result = numerator / denominator;
	return true;
}


int main()
{
	char again = 'y';


	
	while (again == 'y' || again == 'Y')
	{
		cout << "enter two positive whole numbers and one of the following characters (+, -, *, /, ^) ";
		cout << "Number 1: ";
		cin >> num1;
		cout << "Number 2: ";
		cin >> num2;
		cout << "Operator (+, -, *, /) : ";
		cin >> Input;
		if (Input == '+')
		{
			Number = Added(num1, num2);
			cout << "=" << Number << "\n\n";
		}
		else if (Input == '-')
		{
			Number = Subtract(num1, num2);
			cout << "=" << Number << "\n\n";
		}
		else if (Input == '*')
		{
			Number = Multiply(num1, num2);
			cout << "=" << Number << "\n\n";
		}
		if (Divide (num1, num2, Number))
		{
			cout << num1 << " / " << num2 << " = " << Number << "\n";
		}
		else
		{
			cout << "ERROR: Cannot divide by 0. ";
		}

		cout << "To do another one click y. To quit click q. (y/q) :";
		cin >> again;
	} 

	cout << "The end." << endl;

	(void)_getch();
	return 0;
}
